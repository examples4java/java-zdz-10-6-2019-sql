-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Cze 2019, 18:42
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `java`
--
CREATE DATABASE IF NOT EXISTS `java` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `java`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody`
--

DROP TABLE IF EXISTS `kody`;
CREATE TABLE `kody` (
  `id_kod` bigint(20) UNSIGNED NOT NULL,
  `wartosc` char(6) NOT NULL,
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kody`
--

INSERT INTO `kody` (`id_kod`, `wartosc`, `id_miejscowosc`) VALUES
(1, '97-400', 3),
(12, '42-200', 2),
(13, '40-167', 2),
(14, '97-410', 3),
(15, '40-032', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miejscowosci`
--

DROP TABLE IF EXISTS `miejscowosci`;
CREATE TABLE `miejscowosci` (
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `miejscowosci`
--

INSERT INTO `miejscowosci` (`id_miejscowosc`, `nazwa`) VALUES
(1, 'Mysłowice'),
(2, 'Częstochowa'),
(3, 'Bełchatów'),
(5, 'Katowice'),
(6, 'Zawiercie');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(35) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `telefon` varchar(20) NOT NULL,
  `possja` varchar(10) NOT NULL,
  `lokal` varchar(10) NOT NULL,
  `id_kod` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kody`
--
ALTER TABLE `kody`
  ADD UNIQUE KEY `id_kod` (`id_kod`),
  ADD KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indeksy dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD UNIQUE KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_kod` (`id_kod`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kody`
--
ALTER TABLE `kody`
  MODIFY `id_kod` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  MODIFY `id_miejscowosc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `kody`
--
ALTER TABLE `kody`
  ADD CONSTRAINT `kody_ibfk_1` FOREIGN KEY (`id_miejscowosc`) REFERENCES `miejscowosci` (`id_miejscowosc`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_kod`) REFERENCES `kody` (`id_kod`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
